"use client";

import { Swiper, SwiperSlide } from "swiper/react";
import { useRouter } from "next/navigation";
import { useIsFetching } from "@tanstack/react-query";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

// import required modules
import { Autoplay, Navigation } from "swiper/modules";
import Loading from "./loading";
import { useQuery } from "@tanstack/react-query";

import { queryKeys } from "@/constants";
import { getData } from "@/utils";

export default function HomePage() {
  const { data, error } = useQuery({
    queryKey: [queryKeys.products],
    queryFn: getData,
  });
  const router = useRouter();
  const onSlideClick = (slide: any) => {
    router.push(`/${slide.id}`);
  };

  if (error) {
    return <>{error.message}</>;
  }
  if (data) {
    return (
      <>
        {useIsFetching() ? (
          <Loading />
        ) : (
          <div className="h-[500px] shadow-md m-4 rounded-md">
            <Swiper
              spaceBetween={30}
              centeredSlides={true}
              autoplay={{
                delay: 2500,
                disableOnInteraction: false,
              }}
              pagination={{
                clickable: true,
              }}
              navigation={true}
              modules={[Navigation, Autoplay]}
              className="rounded-md"
            >
              {data?.map((slide: any, index: number) => {
                return (
                  <SwiperSlide key={index}>
                    <div className="bg-gray-100">
                      <div className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
                        <div className="mx-auto max-w-2xl py-16 sm:py-24 lg:max-w-none lg:py-32">
                          <h2 className="text-xl font-semibold text-gray-600">
                            {slide.title}
                          </h2>

                          <div className="mt-6 space-y-12 lg:grid lg:gap-x-6 lg:space-y-0">
                            <div className="group relative">
                              <div className="relative h-80 w-full overflow-hidden rounded-lg bg-white sm:aspect-h-1 sm:aspect-w-2 lg:aspect-h-1 lg:aspect-w-1 group-hover:opacity-75 sm:h-64">
                                <img
                                  src={slide.image}
                                  alt={slide.title}
                                  className="h-full w-full object-contain object-center cursor-pointer"
                                  onClick={() => onSlideClick(slide)}
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                );
              })}
            </Swiper>
          </div>
        )}
      </>
    );
  }
}
