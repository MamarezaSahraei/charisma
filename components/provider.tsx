"use client";
import { ReactNode, useState } from "react";
import { QueryClientProvider } from "@tanstack/react-query";
import { customQueryClient } from "@/react-query/queryClient";
import { ReactQueryDevtools } from "@tanstack/react-query-devtools";
export default function Provider({ children }: { children: ReactNode }) {
  const [queryClient] = useState(() => customQueryClient);
  return (
    <>
      <QueryClientProvider client={queryClient}>
        {children}
        <ReactQueryDevtools initialIsOpen={false} />
      </QueryClientProvider>
    </>
  );
}
