import { config } from "./config";
import { queryKeys } from "./queryKeys";
export {
    config,
    queryKeys
}