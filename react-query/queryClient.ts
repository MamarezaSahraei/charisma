import { QueryClient } from "@tanstack/react-query";

export const customQueryClient = new QueryClient({
    defaultOptions: {
        queries: {
            staleTime: 600000,
            refetchOnMount: false,
            refetchOnReconnect: false,
            refetchOnWindowFocus: false,
            refetchInterval: 600000,
        }
    }
});