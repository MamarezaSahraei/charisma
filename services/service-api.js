import Axios from "axios";
import { config } from "@/constants";

const data = {};

const ServiceApi = Axios.create();

ServiceApi.interceptors.request.use(
  async (req) => {
    const isGet = req.method === "get";
    const cache = req.params?.cache;
    const dataKey = `${req.url}${JSON.stringify(req.params)}`;
    const hasData = data[dataKey];
    const needCache = [isGet, cache, hasData].every(Boolean);
    const adapterData = {
      config: req,
      request: req,
      headers: req.headers,
      data: data[dataKey],
      status: 200,
      statusText: "OK",
    };
    if (needCache) {
      req.adapter = () => Promise.resolve(adapterData);
      return req;
    }
    return req;
  },
  (err) => {
    console.log(err);
    return Promise.reject(err);
  }
);
ServiceApi.interceptors.response.use(
  (res) => {
    const key = `${res.config.url}${JSON.stringify(res.config.params)}`;
    const value = res.data;
    data[key] = value;
    return res;
  },
  (err) => {
    console.log(err);
    return Promise.reject(err);
  }
);

export default ServiceApi;
