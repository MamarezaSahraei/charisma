"use client";
import { queryKeys } from "@/constants";
import { useQuery, useQueryClient } from "@tanstack/react-query";
import { getDetails } from "@/utils";
import { Like1, Dislike } from "iconsax-react";

export default function Details({ params }: { params: { id: string } }) {
  const queryClient = useQueryClient();
  const { data, error } = useQuery({
    queryKey: [queryKeys.details, params.id],
    queryFn: () => getDetails(params.id),
  });

  const onIconClick = (status: string) => {
    queryClient.setQueryData([queryKeys.details, params.id], {
      ...data,
      status,
    });
  };

  if (error) {
    return <>{error.message}</>;
  }

  if (data) {
    return (
      <div className="flex min-h-full items-stretch justify-center text-center md:items-center md:px-2 lg:px-4">
        <div className="flex w-full transform text-left text-base transition md:my-8 md:max-w-2xl md:px-4 lg:max-w-4xl">
          <div className="relative flex w-full items-center overflow-hidden bg-white px-4 pb-8 pt-14 shadow-2xl sm:px-6 sm:pt-8 md:p-6 lg:p-8">
            <div className="grid w-full grid-cols-1 items-start gap-x-6 gap-y-8 sm:grid-cols-12 lg:gap-x-8">
              <div className="aspect-h-3 aspect-w-2 overflow-hidden rounded-lg bg-gray-100 sm:col-span-4 lg:col-span-5">
                <img
                  src={data.image}
                  alt="Two each of gray, white, and black shirts arranged on table."
                  className="object-cover object-center"
                />
              </div>
              <div className="sm:col-span-8 lg:col-span-7 space-y-8">
                <h2 className="text-md font-bold text-gray-900 sm:pr-12">
                  Title : {data.title}
                </h2>

                <p className="text-2xl text-gray-900">Price : ${data.price}</p>

                <p className="text-sm text-gray-900">
                  Category : {data.category}
                </p>
                <p className="text-sm text-gray-900">
                  Description : {data.description}
                </p>
                <div className="flex space-x-4">
                  <Like1
                    size="32"
                    className="cursor-pointer"
                    color="#FF8A65"
                    variant={data.status === "like" ? "Bold" : "Outline"}
                    onClick={() => onIconClick("like")}
                  />
                  <Dislike
                    size="32"
                    className="cursor-pointer"
                    color="#FF8A65"
                    variant={data.status === "dislike" ? "Bold" : "Outline"}
                    onClick={() => onIconClick("dislike")}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
