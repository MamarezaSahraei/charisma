import { ServiceApi } from "@/services";

export const getData = async () => {
    const { data } = await ServiceApi.get("https://fakestoreapi.com/products");
    return data;
};

export const getDetails = async (id: string) => {
    const { data } = await ServiceApi.get(`https://fakestoreapi.com/products/${id}`);
    return data
};